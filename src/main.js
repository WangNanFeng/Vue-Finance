import Vue from 'vue'
import App from './App'
import router from './router'
// 自适应
import 'lib-flexible'
// 初始化样式
import '@/publicresource/css/reset.scss'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
