import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
// 引入home组件
const Home = () => import('@/components/home/index')
const Borrow = () => import('@/components/borrow/index')
const Test = () => import('@/components/test/test')

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/borrow',
    name: 'Borrow',
    component: Borrow
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  }
]

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes
})
